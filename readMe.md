- prérequis:
vagrant
ansible

pour lancer la vm à la racine du projet
```shell
bash i.sh
```

sur un navigateur :
```http
localhost:3333
```
ou
```http
192.168.50.4:3333
```